import LoginPage from "./pages/LoginPage";
import { Toaster } from "react-hot-toast";
import CurrenciesPage from "./pages/CurrenciesPage";
import { useState, useEffect } from "react";
function App() {
  const [loggedIn, setLoggedIn] = useState(false);
  const changeLoggedInFlag = (value) => {
    setLoggedIn(value);
  };
  useEffect(() => {
    const currentLoginData = localStorage.getItem("loginData");
    if (currentLoginData) {
      setLoggedIn(true);
    }
  }, []);
  return (
    <div>
      <Toaster position="bottom-center" />

      {loggedIn ? (
        <CurrenciesPage changeLoggedInFlag={changeLoggedInFlag} />
      ) : (
        <LoginPage changeLoggedInFlag={changeLoggedInFlag} />
      )}
    </div>
  );
}

export default App;

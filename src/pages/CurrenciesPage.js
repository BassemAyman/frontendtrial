import React, { useEffect, useState } from "react";
import api from "../api";
import searchIcon from "../assets/searchIcon.png";
function CurrenciesPage({ changeLoggedInFlag }) {
  //state
  const [currencies, setCurrencies] = useState([]);
  const [displayCurrencies, setDisplayCurrencies] = useState([]);
  const [searchText, setSearchText] = useState("");

  //methods
  const fetchCurrencies = async () => {
    try {
      const userData = JSON.parse(localStorage.getItem("loginData"));
      const response = await api().get(`/base/currency/${userData.company}`);
      setCurrencies(response.data);
      setDisplayCurrencies(response.data);
    } catch (error) {
      console.log("error", error);
    }
  };
  const searchCurrencies = (textInput) => {
    setSearchText(textInput);
    if (textInput.length) {
      const matchedCurrencies = currencies.filter((item) => {
        if (textInput.length > 0) {
          return (
            item.name.toLowerCase().match(textInput.toLowerCase()) ||
            item.code.toLowerCase().match(textInput.toLowerCase())
          );
        }
        return true;
      });
      setDisplayCurrencies(matchedCurrencies);
    } else {
      setDisplayCurrencies(currencies);
    }
  };

  //lifecycle
  useEffect(() => {
    fetchCurrencies();
  }, []);

  //render
  return (
    <div className="p-8">
      <div>
        Currencies Page
        <button
          className="w-[100px] h-[50px] bg-stone-500 text-white rounded-[5px] font-bold m-4"
          onClick={() => {
            localStorage.clear();
            changeLoggedInFlag(false);
          }}
        >
          Logout
        </button>
      </div>
      <div className="p-8 flex justify-center text-sm">
        <div className=" min-w-[300px] h-[360px] inline-block rounded-lg border-[1px] border-[#E3E5E5]] pt-[14px]">
          <div className="relative mt-15px">
            <input
              type="text"
              placeholder="Search..."
              className=" w-full h-[48px] pl-[54px] outline-none rounded-lg "
              value={searchText}
              onChange={(e) => {
                searchCurrencies(e.target.value);
              }}
            />
            <img
              src={searchIcon}
              alt="searchIcon"
              className="inline absolute left-[22px] top-1/2 -translate-y-1/2"
            ></img>
          </div>

          <div className="h-[1px] bg-[#E3E5E5] " />

          <div className="max-h-[298px]  overflow-auto rounded-b-lg ">
            {displayCurrencies.map((item) => {
              return (
                <div key={item.id} className="my-[28px] space-x-4 px-4">
                  <input type="radio" className="hover:cursor-pointer" />
                  <span>
                    <img src={item.flag} alt="flag" className="inline"></img>
                  </span>
                  <span>
                    {item.code} - {item.name}
                  </span>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
}

export default CurrenciesPage;

import loginImage from "../assets/loginImage1.jpg";
import googleIcon from "../assets/googleIcon.svg";
import React, { useState } from "react";
import toast from "react-hot-toast";
import api from "../api";
function LoginPage({ changeLoggedInFlag }) {
  const [values, setValues] = useState({
    emailValue: "",
    passwordValue: "",
  });
  const handleSubmitForm = async (e) => {
    e.preventDefault();
    // toast.success("Successfully created!");
    if (!values.emailValue.length) {
      toast.error("Please input email", {
        duration: 4000,
      });
      return;
    }
    if (!values.passwordValue.length) {
      toast.error("Please input password", {
        duration: 4000,
      });
      return;
    }
    let requestBody = {
      contact: values.emailValue,
      password: values.passwordValue,
    };
    try {
      const response = await api().post("/user-management/login/", requestBody);
      localStorage.setItem("loginData", JSON.stringify(response.data));
      toast.success("Logged in successfully", {
        duration: 3000,
      });
      changeLoggedInFlag(true);
    } catch (error) {
      console.log("error", error.response);
      if (error.response?.data?.detail) {
        toast.error(error.response.data.detail, {
          duration: 4000,
        });
      }
    }
  };
  return (
    <>
      <div className="grid sm:grid-cols-1 md:grid-cols-2 border">
        <div className="hidden md:block">
          <div
            className="bg-center bg-no-repeat bg-cover h-screen"
            style={{ backgroundImage: `url(${loginImage})` }}
          />
        </div>
        <div className=" grid grid-cols-7 md:grid-cols-4 h-screen relative">
          <div className="col-start-2 col-span-5 md:col-start-2 md:col-span-2  ">
            <div className="flex items-center h-full relative">
              <div className="w-full">
                <div className="text-base">Welcome back</div>
                <div className="text-3xl font-bold mb-6">
                  Login to your account
                </div>
                <form onSubmit={handleSubmitForm}>
                  <div className="mb-2">Email</div>
                  <div className="mb-4 ">
                    <input
                      type="text"
                      className="w-full h-[50px] border-2 border-[#E8E8E8] rounded-[5px] px-5"
                      value={values.emailValue}
                      onChange={(e) => {
                        setValues({ ...values, emailValue: e.target.value });
                      }}
                    />
                  </div>
                  <div className="mb-2">Password</div>
                  <div className="mb-6">
                    <input
                      type="password"
                      className="w-full h-[50px] border-2 border-[#E8E8E8] rounded-[5px] px-5"
                      value={values.passwordValue}
                      onChange={(e) => {
                        setValues({ ...values, passwordValue: e.target.value });
                      }}
                    />
                  </div>
                  <div className="flex justify-between mb-6">
                    <div>
                      <input type="radio" className="hover:cursor-pointer" />
                      <span className="ml-[11px]">Remember Me</span>
                    </div>
                    <div className="text-[#2C5282] hover:cursor-pointer hover:underline hover:text-[#427abe]">
                      Forgot Password?
                    </div>
                  </div>
                  <div>
                    <button
                      className="w-full h-[50px] bg-[#04C35C] text-white rounded-[5px] font-bold mb-4"
                      type="submit"
                    >
                      Login Now
                    </button>
                  </div>
                </form>
                <div>
                  <button className="w-full h-[50px] bg-[#535454] text-white rounded-[5px] font-bold">
                    <div className="content-center">
                      <img
                        className="inline"
                        src={googleIcon}
                        alt="googleIcon"
                      />
                      <span className="ml-[11px]">Or sign-in with google</span>
                    </div>
                  </button>
                </div>
              </div>
            </div>
          </div>
          <div className="absolute bottom-[50px] left-1/2 -translate-x-1/2">
            Dont have an account?{" "}
            <span className="text-[#2C5282] hover:cursor-pointer hover:underline hover:text-[#427abe]">
              Join free today
            </span>
          </div>
        </div>
      </div>
    </>
  );
}

export default LoginPage;

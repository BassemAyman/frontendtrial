import axios from "axios";

let apiWrapper = () => {
  const tokenExtractor = () => {
    let currentLoginData = localStorage.getItem("loginData");
    if (currentLoginData) {
      let parsedData = JSON.parse(currentLoginData);
      const currentToken = parsedData.access_token;
      return `Bearer ${currentToken}`;
    }
    return null;
  };

  let api = axios.create({
    baseURL: process.env.REACT_APP_API_URL,
    headers: {
      Authorization: tokenExtractor(),
    },
  });
  return api;
};

export default apiWrapper;
